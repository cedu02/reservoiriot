#include <DallasTemperature.h>

#include <OneWire.h>

#include <GravityTDS.h>

#define TdsSensorPin A3

GravityTDS gravityTds;

float temperature = 25,

tdsValue = 0;

#define ONE_WIRE_BUS 2
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

void setup()
{
    Serial.begin(115200);
    sensors.begin();
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(5.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.begin();  //initialization
}

void loop()
{
    //temperature = readTemperature();  //add your temperature sensor and read it
    sensors.requestTemperatures();
    float temp = sensors.getTempCByIndex(0); 
    gravityTds.setTemperature(temp);  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate
    tdsValue = gravityTds.getTdsValue();  // then get the value
    float ecValue = gravityTds.getEcValue();
    Serial.print(tdsValue,0);
    Serial.println("ppm");
    Serial.println((String)"EC: " + ecValue); 
    Serial.println((String)"temp: " + temp);

    delay(1000);
}
