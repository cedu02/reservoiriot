#include <Arduino.h>
#include <SerialCommunication.h>
#include <TDSSensor.h>
#include "../lib/WaterLevel/WaterLevelService.h"
#include "../lib/WaterPump/WaterPump.h"
#include "../lib/UVSensor/UVSensorService.h"
#include "../lib/AirPump/AirPump.h"
#include "../lib/WaterTemperature/WaterTemperatureService.h"
#include "../lib/PHSensor/PHSensor.h"
#include "../lib/Lamp/Lamp.h"

String getSuccessString();
String failedTaskString(const String& message);

void setup() {
    Serial.begin(9600);
    WaterTemperatureService::initialize();
    WaterLevelService::initializeSensors();
    TDSSensor::initialize();
    WaterPump::registerPump();
    Lamp::registerLamp();
    AirPump::registerPump();
}

void loop() {
    TaskEnum task = SerialCommunication::getTask();
    switch (task){
        case TaskEnum::StartWaterPump:
            WaterPump::startPump();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StopWaterPump:
            WaterPump::stopPump();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StartAirPump:
            AirPump::startPump();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StopAirPump:
            AirPump::stopPump();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StartLight:
            Lamp::startLamp();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::StopLight:
            Lamp::stopLamp();
            SerialCommunication::sendResult(getSuccessString());
            break;
        case TaskEnum::ReadWaterLevelReservoir:
            SerialCommunication::sendResult(WaterLevelService::getWaterLevelResponse());
            break;
        case TaskEnum::ReadUVLevel:
            SerialCommunication::sendResult(UVSensorService::getUVLevel());
            break;
        case TaskEnum::ReadWaterTemp:
            SerialCommunication::sendResult(WaterTemperatureService::getWaterTemperature());
            break;
        case TaskEnum::ReadPHValue:
            SerialCommunication::sendResult(PHSensor::getPHMeasurement());
            break;
        case TaskEnum::ReadTDSValue:
            SerialCommunication::sendResult(TDSSensor::getTDSMeasurement());
            break;
        case TaskEnum::IsWaterPumpRunning:
            SerialCommunication::sendResult((String)"{\"Running\": " + WaterPump::isPumpRunning() + " }");
            break;
        case TaskEnum::IsAirPumpRunning:
            SerialCommunication::sendResult((String)"{\"Running\": " + AirPump::isPumpRunning() + " }");
            break;
        case TaskEnum::IsLampOn:
            SerialCommunication::sendResult((String)"{\"Running\": " + Lamp::isLampRunning() + " }");
            break;
        case TaskEnum::NoOpenTask:
            break;
        default:
            SerialCommunication::sendResult(failedTaskString("Not implemented"));
    }
    Serial.flush();
}

String getSuccessString() {
    return (String)"{\"Success\": " + true + " }";
}

String failedTaskString(const String& message) {
    return (String) "{\"Success\": " + false + " Message: " + message + " }";
}
