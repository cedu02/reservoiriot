//
// Created by cedu on 7/23/20.
//

#include "OneWire.h"
#include "DallasTemperature.h"

#ifndef RESERVOIRIOT_WATERTEMPERATURESERVICE_H
#define RESERVOIRIOT_WATERTEMPERATURESERVICE_H


class WaterTemperatureService {
public:
    static String getWaterTemperature();
    static void initialize();
    static float readWaterTemperature();
};


#endif //RESERVOIRIOT_WATERTEMPERATURESERVICE_H
