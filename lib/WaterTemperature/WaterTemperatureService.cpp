//
// Created by cedu on 7/23/20.
//

#include "WaterTemperatureService.h"

#define ONE_WIRE_BUS 2
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

float WaterTemperatureService::readWaterTemperature() {
    sensors.requestTemperatures();
    return sensors.getTempCByIndex(0);
}

void WaterTemperatureService::initialize() {
   sensors.begin();
}

String WaterTemperatureService::getWaterTemperature() {
   return (String)"{\"Temp\": " + WaterTemperatureService::readWaterTemperature() + " }";
}