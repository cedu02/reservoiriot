//
// Created by cedu on 7/22/20.
//

#ifndef RESERVOIRIOT_WATERPUMP_H
#define RESERVOIRIOT_WATERPUMP_H
#include "Arduino.h"

class WaterPump {
public:
    static void startPump();
    static void stopPump();
    static bool isPumpRunning();
    static void registerPump();

};
#endif //RESERVOIRIOT_WATERPUMP_H
