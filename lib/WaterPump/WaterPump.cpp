//
// Created by cedu on 7/22/20.
//

#include "WaterPump.h"

#define in1 3

void WaterPump::startPump() {
    digitalWrite(in1, HIGH);
}

void WaterPump::stopPump() {
    digitalWrite(in1, LOW);
}

void WaterPump::registerPump() {
    pinMode(in1, OUTPUT);
    digitalWrite(in1, LOW);
}

bool WaterPump::isPumpRunning() {
    return digitalRead(in1) == HIGH;
}
