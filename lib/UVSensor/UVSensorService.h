//
// Created by cedu on 7/23/20.
//
#include "Arduino.h"

#ifndef RESERVOIRIOT_UVSENSORSERVICE_H
#define RESERVOIRIOT_UVSENSORSERVICE_H

class UVSensorService {
public:
    static String getUVLevel();
private:
    static const int dataPin = A2;
    static float readUVLevel();
};
#endif //RESERVOIRIOT_UVSENSORSERVICE_H
