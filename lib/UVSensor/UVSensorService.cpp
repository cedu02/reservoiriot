//
// Created by cedu on 7/23/20.
//

#include "UVSensorService.h"

float UVSensorService::readUVLevel() {
   int sensor_value = analogRead(dataPin); // Get raw sensor reading
   auto volts = (float)(sensor_value * 5.0 / 1024.0);
   auto UV_index = (float)(volts * 1.64);
   return UV_index;
}

String UVSensorService::getUVLevel() {
    return (String)"{\"UVLevel\": " + UVSensorService::readUVLevel() + " }";
}