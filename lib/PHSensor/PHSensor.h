//
// Created by cedu on 7/28/20.
//
#include "Arduino.h"

#ifndef RESERVOIRIOT_PHSENSOR_H
#define RESERVOIRIOT_PHSENSOR_H


class PHSensor {
public:
    static String getPHMeasurement();

private:
    static float readPHMeasurement();
};


#endif //RESERVOIRIOT_PHSENSOR_H
