//
// Created by cedu on 7/28/20.
//

#include "PHSensor.h"


String PHSensor::getPHMeasurement() {
    return (String)   "{\"PH\": " + PHSensor::readPHMeasurement() + " }";
}

float PHSensor::readPHMeasurement() {
    const int analogInPin = A0;
    unsigned long int avgValue;
    int buf[10],temp;

    for(int & i : buf)
    {
        i= analogRead(analogInPin);
        delay(10);
    }

    for(int i=0;i<9;i++)
    {
        for(int j=i+1;j<10;j++)
        {
            if(buf[i]>buf[j])
            {
                temp=buf[i];
                buf[i]=buf[j];
                buf[j]=temp;
            }
        }
    }

    avgValue=0;
    for(int i=2;i<8;i++)
        avgValue+=buf[i];
    float pHVol=(float) avgValue* (float)5.0/1024/6;
    return (float)-5.70 * pHVol + (float)22.22;
}