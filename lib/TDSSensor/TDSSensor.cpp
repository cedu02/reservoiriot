//
// Created by Cedu on 26.07.2020.
//

#include "TDSSensor.h"
GravityTDS gravityTds;


void TDSSensor::initialize() {
    gravityTds.setPin(A3);
    gravityTds.setAref(5.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 for 10bit ADC;4096 for 12bit ADC
    gravityTds.begin();  //initialization
}

String TDSSensor::getTDSMeasurement() {
    gravityTds.setTemperature(WaterTemperatureService::readWaterTemperature());  // set the temperature and execute temperature compensation
    gravityTds.update();  //sample and calculate
    return (String)  "{\"TDS\": " + gravityTds.getTdsValue() + ", \"EC\": " + gravityTds.getEcValue() + " }";
}