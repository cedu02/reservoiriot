#include "Arduino.h"
#include "GravityTDS.h"
#include "WaterTemperatureService.h"

#ifndef RESERVOIRIOT_TDSSENSOR_H
#define RESERVOIRIOT_TDSSENSOR_H

class TDSSensor {
public:
    static String getTDSMeasurement();
    static void initialize();

};
#endif //RESERVOIRIOT_TDSSENSOR_H
