//
// Created by cedu on 4/6/20.
//

#ifndef RESERVOIRIOT_LAMP_H
#define RESERVOIRIOT_LAMP_H


class Lamp {
public:
    static void startLamp();
    static void stopLamp();
    static bool isLampRunning();
    static void registerLamp();
};


#endif //RESERVOIRIOT_LAMP_H
