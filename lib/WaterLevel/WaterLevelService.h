//
// Created by cedu on 7/23/20.
#include "WaterLevelSensor.h"
#include "WaterLevels.h"
#include "Arduino.h"

#ifndef RESERVOIRIOT_WATERLEVELSERVICE_H
#define RESERVOIRIOT_WATERLEVELSERVICE_H

class WaterLevelService {
public:
    static String getWaterLevelResponse();
    static void initializeSensors();

private:
    static const int topPowerPin = 6;
    static const int topDataPin = A0;

    static const int bottomPowerPin = 7;
    static const int bottomDataPin = A1;

    static WaterLevels getWaterLevel();
};

#endif //RESERVOIRIOT_WATERLEVELSERVICE_H
