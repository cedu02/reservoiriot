
#include "WaterLevelService.h"

/**
 * Read water level from both sensor and return
 * the water level.
 * @return Water level.
 */
WaterLevels WaterLevelService::getWaterLevel() {

    // if top sensor is wrong always check bottom first!
    WaterLevelHeight bottomHeight = WaterLevelSensor::getWaterLevel(bottomPowerPin, bottomDataPin);
    if(bottomHeight == WaterLevelHeight::Empty){
        return WaterLevels::Empty;
    }

    WaterLevelHeight topHeight = WaterLevelSensor::getWaterLevel(topPowerPin, topDataPin);
    if(topHeight == WaterLevelHeight::Empty){
        if(bottomHeight == WaterLevelHeight::Normal){
            return WaterLevels::HalveFull;
        }else if(bottomHeight == WaterLevelHeight::Low){
            return WaterLevels::NearlyEmpty;
        }else{
            return WaterLevels::Empty;
        }
    }
    return WaterLevels::Full;
}
/**
 * Get water level and create Json response.
 * @return json string with water level result.
 */
String WaterLevelService::getWaterLevelResponse() {
    return (String)"{\"WaterLevel\": " + (int)WaterLevelService::getWaterLevel() + " }";
}

/**
 * Initialize Water level Sensors.
 */
void WaterLevelService::initializeSensors() {
    // Set power PINs as Output.
    pinMode(topPowerPin, OUTPUT);
    pinMode(bottomPowerPin, OUTPUT);

    // Set power PINs to LOW to
    // set Sensor on standby.
    digitalWrite(topPowerPin, LOW);
    digitalWrite(bottomPowerPin, LOW);
}