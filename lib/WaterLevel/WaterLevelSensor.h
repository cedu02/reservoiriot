//
// Created by cedu on 3/16/20.
//
#include <Arduino.h>

#ifndef STRAWBERRZ_WATERLEVELSENSOR_H
#define STRAWBERRZ_WATERLEVELSENSOR_H

enum WaterLevelHeight: int{
    Normal = 250,
    Low = 200,
    Empty = 100
};

class WaterLevelSensor {
public:
    static WaterLevelHeight getWaterLevel(int powerPin, int sensorPin);
private:
    static long readWaterLevel(int powerPin, int sensorPin);
};

#endif //STRAWBERRZ_WATERLEVELSENSOR_H
