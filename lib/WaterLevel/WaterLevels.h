//
// Created by cedu on 7/23/20.
//

#ifndef RESERVOIRIOT_WATERLEVELS_H
#define RESERVOIRIOT_WATERLEVELS_H
enum class WaterLevels: int{
  Full = 0,
  HalveFull = 1,
  NearlyEmpty = 2,
  Empty = 3
};
#endif //RESERVOIRIOT_WATERLEVELS_H
