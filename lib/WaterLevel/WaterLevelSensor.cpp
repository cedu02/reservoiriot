/**
 * Read water level from sensor.
 */
#include "WaterLevelSensor.h"

WaterLevelHeight WaterLevelSensor::getWaterLevel(int powerPin, int sensorPin) {
    long value = WaterLevelSensor::readWaterLevel(powerPin, sensorPin);
    if (WaterLevelHeight::Normal < value) {
        return WaterLevelHeight::Normal;
    } else if (value <= WaterLevelHeight::Empty) {
        return WaterLevelHeight::Empty;
    } else {
        return WaterLevelHeight::Low;
    }
}


long WaterLevelSensor::readWaterLevel(int powerPin, int sensorPin) {
    digitalWrite(powerPin, HIGH);    // Turn the sensor ON
    delay(10);                        // wait 10 milliseconds
    long val = analogRead(sensorPin); // Read the analog value form sensor
    digitalWrite(powerPin, LOW); // Turn the sensor OFF
    return val;
}

