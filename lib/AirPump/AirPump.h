//
// Created by cedu on 7/22/20.
//
#include "Arduino.h"

#ifndef RESERVOIRIOT_AIRPUMP_H
#define RESERVOIRIOT_AIRPUMP_H

class AirPump {
public:
    static void startPump();
    static void stopPump();
    static bool isPumpRunning();
    static void registerPump();

};
#endif //RESERVOIRIOT_AIRPUMP_H
