//
// Created by cedu on 7/22/20.
//

#include "AirPump.h"

#define in1 4

void AirPump::startPump() {
    digitalWrite(in1, HIGH);
}

void AirPump::stopPump() {
    digitalWrite(in1, LOW);
}

void AirPump::registerPump() {
    pinMode(in1, OUTPUT);
    digitalWrite(in1, LOW);
}

bool AirPump::isPumpRunning() {
    return digitalRead(in1) == HIGH;
}
